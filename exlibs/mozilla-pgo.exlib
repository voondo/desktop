# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2011, 2012, 2015 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic freedesktop-desktop xdummy [ phase=build option=pgo ]

myexparam co_project
exparam -v co_project co_project

# This exparam allows you to specify that the source will be built with
# unofficial branding to allow binary distribution. The codename replaces
# the official branding in the UI when the bindist option is enabled.
# Consult the "Codename" column here: http://en.wikipedia.org/wiki/Firefox#Release_history
# More on Firefox trademark issues: http://en.wikipedia.org/wiki/Firefox#Trademark_and_logo
myexparam codename
exparam -v codename codename

export_exlib_phases pkg_setup src_prepare src_configure src_compile src_install

MYOPTIONS="
    bindist [[ description = [ Disable official branding, allowing binary redistribution ] ]]
    (
        debug [[ description = [ Disables optimization, enables assertions and other debug-only code ] ]]
        pgo   [[ description = [ Do a second build after profiling to increase runtime performance ] ]]
    ) [[
        number-selected = at-most-one
        note = [ at-most-one because I felt it wasn't worth my time to figure out if it
                 was possible to build some kind of weird unoptimized-optimized monster.
                 It doesn't make much sense if you ask me, but patches are welcome. ]
    ]]
"

mozilla-pgo_pkg_setup() {
    unset \
        DBUS_SESSION_BUS_ADDRESS \
        DISPLAY SESSION_MANAGER \
        XAUTHORITY \
        XDG_SESSION_COOKIE
}

mozilla-pgo_src_prepare() {
    default

    edo sed \
        -e "/^includedir/ c includedir = /usr/$(exhost --target)/include/${PN}" \
        -e "/^idldir/ c idldir = /usr/share/idl/${PN}" \
        -e "/^installdir/ c installdir = /usr/$(exhost --target)/lib/${PN}" \
        -e "/^sdkdir/ c sdkdir = /usr/$(exhost --target)/lib/${PN}-devel" \
        -i "config/baseconfig.mk"
}

mozilla-pgo_src_configure() {
    local x

    # Needed to compile in a chroot environment
    export SHELL=/bin/bash

    local MOZILLA_AC_OPTIONS=(
        # econf can't be used because mozilla's build system is running the ./configure for us.
        # As a result we need to duplicate exherbo's defaults otherwise set by econf.
        # Removed from these defaults are datarootdir and docdir because they where --hates.
        --prefix=/usr/$(exhost --target)
        --target=$(exhost --target)
        --host=$(exhost --build)

        --disable-debug-symbols
        --disable-strip
        --enable-application="${co_project}"
        --with-distribution-id=org.exherbo
        --with-system-nspr
        --with-system-nss
        $(option_enable debug)
        $(option_enable pgo tests)
    )

    local MOZILLA_MK_OPTIONS=(
        MOZ_CO_PROJECT="${co_project}"
        MOZ_OBJDIR=${WORKBASE}/build
        PKG_SKIP_STRIP=1
        TOOLCHAIN_PREFIX=$(exhost --tool-prefix)
    )
    # thunderbird: MOZ_SERVICES_HEALTHREPORT=1 can not be set by mozconfig
    if [[ ${PN} == firefox ]] ; then
        MOZILLA_MK_OPTIONS+=(
            # Workaround for build failures with jobs >~10
            MOZ_MAKE_FLAGS="\"-j${EXJOBS} -O\""
            # Avoids silly notifications about the progress of the build
            MOZ_NOSPAM=1
            MOZ_SERVICES_HEALTHREPORT=1 # fix search bar and enables the missing healthreport page
        )
    else
        MOZILLA_MK_OPTIONS+=(
            MOZ_MAKE_FLAGS="-j${EXJOBS}"
        )
    fi

    # Override the default optimization levels of Mozilla projects with the
    # value specified by -O from the user's CFLAGS/CXXFLAGS (if -O isn't given
    # gcc defaults to '-O0'). Optimization must be disabled when the 'debug'
    # option is set. tests must be enabled for profiling against.
    if option debug; then
        MOZILLA_AC_OPTIONS+=( --enable-debug --disable-optimize --enable-tests )
    else
        local param_optimize
        for x in ${CFLAGS}; do
            case ${x} in
                -O)  param_optimize="--enable-optimize=-O1"  ;;
                -O*) param_optimize="--enable-optimize=${x}" ;;
            esac
        done
        [[ -z ${param_optimize} ]] && param_optimize="--enable-optimize=-O0"
        MOZILLA_AC_OPTIONS+=( --disable-debug "${param_optimize}" $(option_enable pgo tests) )
    fi

    if option bindist; then
        MOZILLA_AC_OPTIONS+=(
            --disable-official-branding
            # don't use --without-branding because it will result in an error
            --with-branding="${co_project}"/branding/unofficial
        )
    else
        MOZILLA_AC_OPTIONS+=( --enable-official-branding )
        MOZILLA_MK_OPTIONS+=( BUILD_OFFICIAL=1 MOZILLA_OFFICIAL=1 )
    fi

    # do this last so exhereses can override any above options
    MOZILLA_AC_OPTIONS+=(
        "${MOZILLA_SRC_CONFIGURE_PARAMS[@]}"
        "${@}"
        $(for s in "${MOZILLA_SRC_CONFIGURE_OPTIONS[@]}"; do
            option ${s}
        done)
        $(for s in "${MOZILLA_SRC_CONFIGURE_OPTION_ENABLES[@]}"; do
            option_enable ${s}
        done)
        $(for s in "${MOZILLA_SRC_CONFIGURE_OPTION_WITHS[@]}"; do
            option_with ${s}
        done)
    )

    for x in "${MOZILLA_AC_OPTIONS[@]}"; do
        edo echo ac_add_options "${x}" >> mozconfig
    done

    for x in "${MOZILLA_MK_OPTIONS[@]}"; do
        edo echo "export         ${x}" >> mozconfig
        edo echo "mk_add_options ${x}" >> mozconfig
    done

    edo ./mach configure
}

mozilla-pgo_src_compile() {
    if option pgo; then
        xdummy_start
        edo ./mach build --verbose
        local ret=${?}
        xdummy_stop
        [[ ${ret} == 0 ]] || die "emake returned error ${ret}"
    else
        edo ./mach build --verbose
    fi
}

mozilla-pgo_src_install() {
    edo pushd "${WORKBASE}"/build
    # Mach is utterly stupid and doesn't allow to pass DESTDIR
    emake DESTDIR="${IMAGE}" install
    edo popd
    emagicdocs

    insinto /usr/share/applications
    doins "${FILES}"/${PN}.desktop

    local icondir

    if option bindist ; then
        edo sed -e "/^Name=/ c Name=${codename}" \
                -i "${IMAGE}"/usr/share/applications/${PN}.desktop
        icondir="${co_project}"/branding/unofficial
    else
        case "${PN}" in
            firefox) icondir="${co_project}"/branding/official ;;
            thunderbird) icondir="${co_project}"/branding/${PN} ;;
        esac
    fi

    local iconname=default64.png

    insinto /usr/share/pixmaps
    newins "${icondir}"/${iconname} ${PN}-icon.png
}

