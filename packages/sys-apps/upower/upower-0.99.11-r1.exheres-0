# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

UPLOADS_ID="93cfe7c8d66ed486001c4f3f55399b7a"

require gitlab [ prefix=https://gitlab.freedesktop.org tag=${PN^^}_$(ever replace_all _ ) suffix=tar.bz2 ] \
    66-service \
    systemd-service \
    udev-rules \
    option-renames [ renames=[ 'systemd providers:systemd' ] ]

SUMMARY="An abstraction for enumerating power devices"
HOMEPAGE+=" https://${PN}.freedesktop.org"
DOWNLOADS="https://gitlab.freedesktop.org/${PN}/${PN}/uploads/${UPLOADS_ID}/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    idevice [[ description = [ Enable support for iPod, iPad, and iPhone battery status ] ]]
    ( linguas: fr it pl sv )
    ( providers: elogind pm-utils systemd ) [[
        *description = [ Suspend/Resume provider ]
        number-selected = at-most-one
    ]]
"

# uses system dbus socket
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.21]
        gobject-introspection? ( gnome-desktop/gobject-introspection[>=0.9.9] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.11] )
    build+run:
        dev-libs/glib:2[>=2.38.0]
        dev-libs/libusb:1
        gnome-desktop/libgudev[>=147]
        idevice? (
            app-pda/libimobiledevice[>=0.9.7]
            dev-libs/libplist[>=0.12]
        )
    run:
        providers:elogind? ( sys-auth/elogind )
        providers:pm-utils? ( sys-power/pm-utils[>=1.4.1] )
        providers:systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --enable-nls
    --disable-static
    --with-backend=linux
    --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
    --with-udevrulesdir=${UDEVRULESDIR}
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    gtk-doc
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    idevice
)

src_install() {
    default

    install_66_files

    # history directory is created at runtime if missing
    edo rm -r "${IMAGE}"/var
}

pkg_postinst() {
    local cruft=( /etc/dbus-1/system.d/org.freedesktop.UPower.conf )
    for file in ${cruft[@]}; do
        if test -f "${file}" ; then
            nonfatal edo rm "${file}" || ewarn "removing ${file} failed"
        fi
    done
}

